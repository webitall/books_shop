<?php

function free_people_scripts()
{

    wp_enqueue_style('style', get_stylesheet_uri());

    wp_enqueue_style('bootstrap-style', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css', array(), '4.1.3');

    wp_enqueue_script('bootstrap-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js', array('jquery'), '4.1.3', true);

    wp_enqueue_style( 'https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css', array() );

    wp_enqueue_script('https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', array(), true);

    wp_enqueue_script ('https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js', array(), true);

    wp_enqueue_script('free_people-js.js', get_template_directory_uri() .'/js.js',array(), '1.0', true);
}

add_action('wp_enqueue_scripts', 'free_people_scripts');

function free_people_register_init_actions()
{

    register_nav_menus(array(
        'primary'   => 'Primary Menu',
        'footer'    => 'Footer Menu',

    ));

    add_theme_support('post-thumbnails');
    /*set_post_thumbnail_size(570, 177 );*/
}

add_action('init', 'free_people_register_init_actions');

function register_my_widgets()
{
    register_sidebar(array(
        'name'          => "Правая боковая панель сайта",
        'id'            => 'right-sidebar',
        'description'   => 'Эти виджеты будут показаны в правой колонке сайта',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));
}

add_action('widgets_init', 'register_my_widgets');


$posts = get_posts( array(
    'numberposts' => 2,
    'category'    => 0,
    'orderby'     => 'date',
    'order'       => 'DESC',
    'include'     => array(),
    'exclude'     => array(),
    'meta_key'    =>    '',
    'meta_value'  =>    '',
    'post_type'   => 'post',
    'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
) );
add_action( 'genesis_meta', 'wpb_add_google_fonts', 5);

function wpb_add_google_fonts()
{
    echo '<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300i,700" rel="stylesheet">';
}
add_filter( 'excerpt_length', function(){
    return 40;
} );


// register Foo_Widget widget
function register_foo_widget() {
    register_widget( 'Foo_Widget' );
}
add_action( 'widgets_init', 'register_foo_widget' );

/*form + textarea*/
add_action( 'widgets_init', function(){
    register_widget( 'TutsplusText_Widget' );
});


/*function my_func( $atts,$) {
    $params = shortcode_atts(array(
        'item' => 'img6.png',
        'item2' => 'image17.png',
        'item3' => 'Photoface2@1X.png',
    ),$atts);

    foreach ( $params as  $param){

        echo "<img src='/img/.{$param}'>";
    }

    /*var_dump($params);*/

