
$(function(){
    $('.bxslider').bxSlider({
        auto: true,
        autoControls: true,
        stopAutoOnClick: true,
        mode: 'fade',
        captions: true,
        slideWidth: 600
    });
});