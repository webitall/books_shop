<?php
/*
Template Name: Special template;
*/
get_header();
?>
    <div class="container">
        <div class="row">
            <div class="special_page"></div>
   <!-- <p><?php /*echo get_ */?></p>-->
<?php


        while ( have_posts() ) :
            the_post();
            the_content();
            /*get_template_part( 'template-parts/content', 'page' );*/

            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) :
                comments_template();
            endif;

        endwhile; // End of the loop.
        ?>
        <?php echo get_post('post'); ?>



        <p class="page_title"><?php echo the_title(); ?></p>
    </div>
    <aside class="siderbar col-lg-3">
        <div class="widget" id="siderbar">
            <?php
            if ( function_exists('dynamic_sidebar') )
                dynamic_sidebar('right-sidebar');
            ?>
        </div>
    </aside>
<?php
get_footer();

