<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Free_people
 */

get_header();
?>
    <div class="container">
        <div class="row">
        <div id="primary" class="content-area col-lg-9">
            <main id="main" class="site-main">
                 <div class="content_part" style="background-image: url('<?php echo get_the_post_thumbnail_url($post->ID); ?>')">
                    <?php
                    while ( have_posts() ) :
                        the_post();

                        /*get_template_part( 'template-parts/content', 'page' );*/

                        // If comments are open or we have at least one comment, load up the comment template.
                        if ( comments_open() || get_comments_number() ) :
                            comments_template();
                        endif;

                    endwhile; // End of the loop.
                    ?>

                     <?php echo get_post('post'); ?>

                     <p class="content_text"><?php echo get_the_content(); ?></p>

                     <p class="page_title"><?php echo the_title(); ?></p>
                 </div>
            </main>
        </div>
                    <aside class="siderbar col-lg-3">
                        <div class="widget" id="siderbar">
                            <?php
                            if ( function_exists('dynamic_sidebar') )
                                dynamic_sidebar('right-sidebar');
                            ?>
                        </div>
                    </aside>


    <div class="date_author col-lg-12"> <p>Пост опубликован: <?php the_date(); ?></p><?php the_author_posts_link(); ?> </div>
        </div>
    </div>
<?php
/*get_sidebar();*/
get_footer();
