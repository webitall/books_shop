
<footer class="site-footer col-lg-12">
    <p class="footer_credits"><?php bloginfo('name'); ?> - &copy; <?php echo date('Y'); ?></p>
        <div class="footer_nav">
            <?php
            $arg = array (
                'theme_location' => 'footer'
            );

            ?>
            <?php wp_nav_menu( $arg ); ?>
        </div>


    </div>
    </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<script>
    $(function(){
        $('.bxslider').bxSlider({
            auto: true,
            autoControls: false,
            stopAutoOnClick: true,
            mode: 'fade',
            captions: false,
            slideWidth: 600
        });
    });
</script>
<?php wp_footer(); ?>
</body>
</html>
