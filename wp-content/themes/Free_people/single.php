<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Mobile_technology
 */
echo $res;
get_header();
?>
    <div class="container">
        <div class="row">
            <div id="primary" class="content-area col-lg-9">
                <main id="main" class="post-block">
                    <div class="post_content">

                        <?php
                    while ( have_posts() ) :
                        the_post();

                        get_template_part( 'template-parts/content', get_post_type() );

                         the_post_navigation('0');

                        // If comments are open or we have at least one comment, load up the comment template.
                        if ( comments_open() || get_comments_number() ) :
                            comments_template();
                        endif;

                    endwhile; // End of the loop.
                    ?>


                    </div>

                </main><!-- #main -->
            </div>
            <aside class="siderbar col-lg-3">
                <div class="widget" id="siderbar">
                    <?php
                    if ( function_exists('dynamic_sidebar') )
                        dynamic_sidebar('right-sidebar');

                    ?>
                </div>
            </aside><!-- #primary -->
        </div>
    </div>
<?php
get_sidebar();
get_footer();
