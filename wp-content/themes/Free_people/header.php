<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php bloginfo('name'); ?></title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">

    <?php wp_head(); ?>


</head>
<body <?php body_class(); ?> >
    <div class="container">
        <div class="row">
            <header class="site-header col-lg-12">
                    <a href=" <?php bloginfo('url'); ?>" alt="logo" class="logo col-lg-3"> <img class="logo" src="<?php echo get_template_directory_uri() ?>/Phoface.png"> </a>
               <div class="title-wrap col-lg-9">
                    <h1> <a class="site-name" href="<?php echo home_url(); ?>">  <?php bloginfo('name'); ?> </a> </h1>
                    <h5 ><?php bloginfo('description') ?> <?php if(is_page('about')){?>
                        -подзаголовок <?php }?></h5>

               </div>
                    <nav class="site_header_nav col-lg-12">
                         <?php
                            $arg = array (
                                    'theme_location' => 'primary'
                            );
                         ?>

                        <?php wp_nav_menu( $arg ); ?>

                     </nav>
            </header>
