<?php
    get_header();
    ?>
    <!--<div class="main-page">-->
        <div class="blog-page d-flex justify-content-center col-lg-12">
<?php

    if( have_posts()) :
        while (have_posts() ) : the_post(); ?>

            <article class="post col-lg-5" style="background-image: url('<?php echo get_the_post_thumbnail_url($post->ID); ?> ')">
    
                <a href="<?php the_permalink(); ?>">
                    <p class="desc"> <?php echo get_the_excerpt(); ?>read more</p>
                </a>
                <h1><a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a></h1>
                <a href="<?php the_permalink(); ?>">
                    <p class="post_author"><?php the_author_link(); ?> - <?php the_time('j F Y '); ?></p>
                </a>

        </article>

        <?php

        endwhile;

        else :
            echo '<p>No content found</p>';

        endif;


?>
        </div>
    </div>

<?php get_footer();

;?>