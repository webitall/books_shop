<?php
get_header();

$events = new WP_Query(array('post_type' => 'events') );

while( $events->have_posts() ){

    $events->the_post();

    $currentEv = get_post();

   if( get_field('text_2',$currentEv->post_content) ): ?>

       <span><?php the_field('text_2', $currentEv->post_content); ?></span>

   <?php endif;

};

get_footer(); ?>
