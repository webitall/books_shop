<?php
/*
Template Name: Search Page
*/

get_header(); ?>

    <div class="search_wrap" style="background-image: url('<?php echo get_the_post_thumbnail_url($post->ID); ?>')">
        <div id="primary" class="content-area search_area">
            <main id="main" class="site-main" role="main">
                <?php get_search_form(); ?>

            </main><!-- #main -->
        </div><!-- #primary -->
        <div class="search_result">

        </div>
    </div><!-- .wrap -->

<?php get_footer();