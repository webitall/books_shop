<?php get_header();

?>

<div class="single" id="single">
    <div class="single_content" >
        <div class="propose_block col-lg-4 offset-lg-1">
            <img src="<?php echo get_the_post_thumbnail_url($prod->ID); ?>">
            <div class="links_block top">
                <div class="link_box"><img src="<?php echo get_template_directory_uri() .'/img/odnkls.png" alt="soc_img'?>"></div>
                <div class="link_box"><img src="<?php echo get_template_directory_uri() .'/img/vk.png" alt="soc_img'?>"></div>
                <div class="link_box"><img src="<?php echo get_template_directory_uri() .'/img/fb.png" alt="soc_img'?>"></div>
                <div class="link_box"><img src="<?php echo get_template_directory_uri() .'/img/twit.png" alt="soc_img'?>"></div>
            </div>
            <div class="links_block bottom">
                <div class="link_box"><img src="<?php echo get_template_directory_uri() .'/img/insta.png" alt="soc_img'?>"></div>
                <div class="link_box"><img src="<?php echo get_template_directory_uri() .'/img/telega.png" alt="soc_img'?>"></div>
                <div class="link_box"><img src="<?php echo get_template_directory_uri() .'/img/vib.png" alt="soc_img'?>"></div>
                <div class="link_box"><img src="<?php echo get_template_directory_uri() .'/img/hz.png" alt="soc_img'?>"></div>
            </div>
        </div>
        <div class="propose_content col-lg-6">
            <span>#</span>
            <h1><?php the_title(); ?></h1>

            <div class="propose_content_text">
                <?php

                    while ( have_posts() ) : the_post(); ?>

                        <?php wc_get_template_part( 'content', 'aa' ); ?>

                    <?php endwhile; // end of the loop. ?>
                <?php the_content(); ?>
            </div>
            <div class="prise">
                <p>Цена:</p><br>
                <p><?php
                    $product_id = get_the_ID();
                    $product = wc_get_product( $product_id );
                         echo $product->get_regular_price();
                ?>
                </p>
                <p>₽</p>
            </div>
    </div>
</div>
<?php get_footer();
