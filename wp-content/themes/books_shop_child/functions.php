<?php
function books_shop_scripts(){


    wp_enqueue_style( 'bootstrap-style', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css', array(), '4.1.3');

    wp_enqueue_script('bootstrap-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js', array('jquery'), '4.1.3', true);

    wp_enqueue_script('fontawesome-css', 'https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">');

    wp_enqueue_script('js-js' ,get_template_directory_uri() . '/js/jquery.arcticmodal-0.3.min.js', array('jquery'), '', true);

    wp_enqueue_script('common-js' ,get_template_directory_uri() . '/js/js.js', array('jquery'), '', true);

    wp_enqueue_style( 'arcticmodal', get_template_directory_uri().'/style/jquery.arcticmodal-0.3.css');

    wp_enqueue_style( 'arcticmodal-css', get_template_directory_uri().'/style/simple.css');

    wp_enqueue_style( 'single-style.css', get_template_directory_uri().'/single.style.css');

    wp_enqueue_style( 'style', get_stylesheet_uri());

    wp_enqueue_style( 'media-css.css', get_template_directory_uri().'/media/media.css');

    wp_localize_script( 'common-js', 'wordpressAjax', [
            'url' => admin_url( 'admin-ajax.php' ),
    ] );
}
add_action('wp_enqueue_scripts', 'books_shop_scripts');

function books_shop_register_init_actions() {

    register_nav_menus(array(
        'top_menu' => 'Top_menu',
        'footer'   => 'footer_menu',
    ));
}

add_action('init', 'books_shop_register_init_actions');


function myFunc(){
    $searchQuery = $_POST['searchQuery'];
    if( empty($searchQuery)){
        wp_send_json([
           'error' => 'pusto',
        ]);
    }

    $foundPosts = new WP_Query([
        's'         => trim($searchQuery),
        'post_type' => 'post',
    ]);

    if( empty ($foundPosts->posts)){
        wp_send_json([
            'error' => 'net postov',
        ]);
    }
    $html = renderPostsHTML($foundPosts->posts);

    wp_send_json([
        'searchQuery' => $searchQuery,
        'html'        => $html,
    ]);
};

add_action('wp_ajax_nopriv_myFunc', 'myFunc');
add_action('wp_ajax_myFunc', 'myFunc');

function renderPostsHTML(array $posts) {
    if ( empty($posts)){
        return '';
    }

    ob_start();
    echo '<section>';
    foreach($posts as $post){
        ?>
        <div class="post_wrapper">
            <h2><?php echo $post->post_title; ?></h2>
            <p><?php echo $post->post_content; ?></p>
        </div>
        <?php
    }
    echo '</section>';
    return ob_get_clean();
};


function register_post_types(){
    $labels = array(
        'name'               => _x( 'Events', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Event', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Events', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Events', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New', 'book', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All Events', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search Events', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent Events:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No events found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No events found in Trash.', 'your-plugin-textdomain' )
    );

    $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'event' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
    );

    register_post_type( 'events', $args );
};

add_action( 'init', 'register_post_types' );

                                                //SOCIAL ICONS WIDGET SIDEBAR
function myWidget(){
    register_sidebar(array(
            'name'         => 'Виджет',
            'id'           =>   'widget',
            'description'  => 'under img',
            'before_widget' => '<li>',
            'after_widget'  => '</li>',
    ));
}
add_action('init', 'myWidget');

add_filter( 'excerpt_length', function(){
    return 20;
} );