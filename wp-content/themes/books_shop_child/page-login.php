<?php
    get_header();
?>
<div class="sign_modal">
    <div class="modal_type">
        <h3>Авторизация</h3>
        <a href="#"><h4>Регистрация</h4></a>
        <a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri().'/img/close.png'?>" alt="x"></a>
    </div>
    <hr>
    <div class="modal_form" style="display: flex; flex-direction: column">
        <label for="email">Электронная почта</label>
        <input type="email" id="modal_email" class="modal_email" placeholder="mironova@mail.ru">
        <label for="password">Пароль:</label>
        <input type="password" id="pass" class="pass" placeholder="***">
        <img src="<?php echo get_template_directory_uri().'/img/pass.png'?>">
    </div>
    <div class="remember_foget">
        <div class="remember">
            <input type="checkbox" id="check">
            <p>запомнить меня</p>
        </div>
        <a href="#">Забыли пароль</a>
    </div>
        <div class="modal_buttons">
            <a href="#" class="enter">войти</a>
            <a href="#" class="out">отмена</a>
        </div>
    <p class="in_soc">войти через</p>
    <div class="soc_btn">
        <a href="#" class="face_btn soc">
            <img src="<?php echo get_template_directory_uri().'/img/f.png'?>" alt="">
            facebook
        </a>
        <a href="#" class="vk_btn soc">
            <img src="<?php echo get_template_directory_uri().'/img/vk_form.png'?>" alt="">
           вконтакте
        </a>
    </div>
</div>

<?php
get_footer();