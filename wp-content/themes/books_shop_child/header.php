
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <title><?php bloginfo('name'); ?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <div class="first_screen">
        <div class="wrap">
            <div class="container">
                <div class="row">
                    <header>
                        <div class="header">
                            <div class="logo">
                                <a href="<?php echo home_url(); ?>"><img class="logo_img" src="<?php echo get_template_directory_uri() . '/img/logo.png'?>"></a>
                                    <p class="menu"><a href="#">menu</a></p>
                                    <img class="burger_menu" src="<?php echo get_template_directory_uri() . '/img/menu.png'?>">
                            </div>
                            <div class="nav">
                                <?php
                                $arg = array (
                                    'theme_location' => 'top_menu'
                                );
                                ?>
                                <?php wp_nav_menu( $arg ); ?>
                            </div>
                            <div>
                                <div class="log_in">
                                    <a href="/login/"><img class="login" src="<?php echo get_template_directory_uri() . '/img/logIn.png'?>"></a>
                                    <p><a href="/login/">войти</a></p>
                                    <a href="/search/"><img class="search" src="<?php echo get_template_directory_uri() . '/img/search.png'?>"></a>
                                        <!--ARCTIC MODAL-->
                                        <div style="display: none;">
                                            <div class="box-modal" id="exampleModal">
                                                <div class="box-modal_close arcticmodal-close">закрыть</div>
                                                Пример модального окна
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </header>