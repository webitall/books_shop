
(function($){
    $(document).on('ready', function() {
        let form = $('#searchform');
         form.on('submit', function(e) {
            e.preventDefault();
           let formdata = new FormData();

             formdata.append('action', 'myFunc');
            formdata.append( 'searchQuery', $('#ss').val());
            $.ajax({
                type: 'POST',
                url: wordpressAjax.url,
                data: formdata,
                processData: false,
                contentType: false,
                success: function (response){
                    if ( ! response.error ){
                        $('.search_result').html(response.html).css('color', 'black');
                    } else {
                        $('.search_result').html(response.error).css('color', 'red');
                    }
                }
            })
        })
    });

})(jQuery);


/*
(function($){
    $(document).on('ready', function(){
       let forma = $('#searchform');
       forma.on('submit', function(e){
           e.preventDefault();

           let formdata = new FormData();
           formdata.append('action','aja');
           formdata.append('querySearch', $('#ss').val() );
           $.ajax({
               type: 'POST',
               url: wordpressAjax.url,
               data: formdata,
               processData: false,
               contentType: false,
               success: function(response){
                   if ( ! response.error ) {
                       $('.search_result').html(response.view);
                   }
               }
           })
       })
    });
}(jQuery));*/
