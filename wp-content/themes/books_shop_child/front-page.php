<?php get_header();
?>
                <div class="represent">
                            <?php
                            $myposts = get_posts( array(
                                'post_type' => 'events'
                            ) );
                            foreach( $myposts as $post ){

                            ?>
                    <div class="represent_block">
                            <img  alt="img" src="<?php the_field('test_1',$post->ID ); ?>">

                            <div class="represent_text">

                                    <h4><?php the_title(); ?></h4>

                                    <a href="<?php the_permalink($post->post_excerpt); ?>">

                                    <p><?php echo $post->post_content; ?></p>

                                     <?php if( get_field('text1', $post->ID) ): ?>
                                         <span><?php the_field('text1', $post->ID ); ?></span>
                                     <?php endif; ?>
                                 </a>
                            </div>
                    </div>
                        <?php
                        }
                            wp_reset_postdata();

                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<article id="second_screen">
    <div class="container">
        <div class="row">
            <div class="under_first_screen col-lg-12">
                <div class="about_complect">
                    <p><a href="#">О комплекте</a></p>
                </div>
                <div class="buy_complect">
                    <img src="<?php echo get_template_directory_uri() . '/img/basket_grey.png'?>">
                    <p><a href="#">купить комплект</a></p>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="container">
        <div class="row">
            <div class="propose_block col-lg-4 offset-lg-1">
                <?php                                                           /*Вывод через поле картинку товара*/
                $prod = get_field('prod');
                    if( $prod ){?>
               <img src="<?php echo get_the_post_thumbnail_url($prod->ID); ?>">

                    <div class="links_block top">

                        <?php                                                       /*SIDEBAR SOC ICONS*/
                        if ( function_exists('myWidget') )
                            dynamic_sidebar('widget'); ?>

                    </div>
               <!-- <div class="links_block bottom">
                    <div class="link_box"><img src="<?php /*echo get_template_directory_uri() .'/img/insta.png" alt="soc_img'*/?>"></div>
                    <div class="link_box"><img src="<?php /*echo get_template_directory_uri() .'/img/telega.png" alt="soc_img'*/?>"></div>
                    <div class="link_box"><img src="<?php /*echo get_template_directory_uri() .'/img/vib.png" alt="soc_img'*/?>"></div>
                    <div class="link_box"><img src="<?php /*echo get_template_directory_uri() .'/img/hz.png" alt="soc_img'*/?>"></div>
                </div>-->
            </div>

            <div class="propose_content col-lg-6">
                <span>#</span>
                 <h1><?php the_title(); ?></h1>
                    <p class="propose_content_text">

             <?php echo $prod->post_content; ?>
                </p>
                       <div class="prise">
                           <p>Цена:</p><br>
                               <?php
                               $prod = get_field('prod');
                               $product = wc_get_product( $prod);
                                echo ($product->get_regular_price());
                                ?>
                             <p>₽</p>
                        </div>

                <?php } ?>
                <div class="propose_content_info cash_back">
                    <p>Cash back 100% </p>
                    <a href="#info_modal"> <img src="<?php echo get_template_directory_uri() . '/img/info.png'?>"></a>
                    <div id="info_modal">
                        <p>Lorem ipsum dl velit voluptatibus? Deleniti distinctio labore laborum omnis sunt.</p>
                    </div>

                </div>
                <div class="propose_content_info buyer">
                    <img src="<?php echo get_template_directory_uri() . '/img/book.png'?>">
                    <p>Статус: Постоянный читатель
                    </p>
                    <img src ="<?php echo get_template_directory_uri() . '/img/info.png'?>">
                </div>
                <div class="propose_content_info money">
                    <img src="<?php echo get_template_directory_uri() . '/img/gold_pig.png'?>">
                    <p>Зарабатывай с нижным клубом Ос
                    </p>
                    <img src ="<?php echo get_template_directory_uri() . '/img/info.png'?>">
                </div>
                <div class="buttons">
                    <a href="#" class="sale_btn B">
                        <img src="<?php echo get_template_directory_uri() . '/img/gold_basket.png'?>">
                        <p class="btn_text">купить комплект</p>
                    </a>
                    <a href="#" class="books_btn B">
                        <img src="<?php echo get_template_directory_uri() . '/img/bookstorage.png'?>">
                        <p class="btn_text">книги комплекта</p>
                    </a>
                </div>

            </div>
        </div>
    </div>
</article>
<article id="drow">
    <div class="wrap">
        <div class="container">
            <div class="row">
                <div class="drow">
                    <span>#</span>
                    <h1>Розыгрыш/ покупателям полного пакета</h1>
                    <p>с 5 апреля по 5 июня 2018</p>
                        <div class="drow_boxes">
                            <?php

                            if (have_rows('my_stock_img')):

                                while (have_rows('my_stock_img')) : the_row();?>

                                    <div class="drow_box">
                                        <a href="<?php get_the_permalink();?>"><img src="<?php echo get_sub_field('my_stock_img_2'); ?>"></a>
                                    </div>
                            <?php
                                endwhile;
                            else :
                                echo 'no rows found';
                            endif;
                            ?>
                        </div>

                    <div class="stock_description">
                        <?php
                            if (have_rows('stock_content')):
                                while(have_rows('stock_content')) :  the_row(); ?>

                                    <p class="stock_text"> <?php echo get_sub_field('my_stock_cont');  ?></p>

                                <?php endwhile;
                            endif;
                        ?>
                            <!--<div class="car_text">
                                <h3>KIA SORENTO</h3>
                                <p class="drow_box_text">Главный приз розыгрыша</p>
                            </div>

                            <div class="tablet_text">
                                <h3>1000 планшетов</h3>
                                <p class="drow_box_text">Читай везде где хочешь</p>
                            </div>-->
                    </div>

                    </div>
                    <div class="more">
                        <a href="#" class="B"><p>Подробнее о розыгрыше</p>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</article>
<article id="books_set">
    <h1>Комплект из 5 книг<span>#</span>Архитектура, интерьер</h1>
    <p>с 5 апреля по 5 июня 2018</p>
    <div class="container">
        <div class="row">
                <?php
                    $prods = get_posts(array(
                            'post_type' => 'product',
                            'include' => '409',
                    ));
                   foreach ($prods as $prod){
                ?>
            <div class="books_set_cont">
                <div class="book_img">
                    <img class="" src="<?php echo get_the_post_thumbnail_url($prod->ID); ?>">
                </div>
                <div class="books_set_cont_text">
                    <h4><?php echo $prod->post_title;?></h4>
                    <p>
                        <?php echo $prod->post_content;?></p>
                    <div>
                        <a class="B" href="<?php the_permalink($prod->ID); ?>"><p>Подробнее о книге</p></a>
                    </div>
                </div>
            </div>
<?php }?>
            <div class="books_list">
                    <?php
                    $prod_ids = get_field('prod2');

                    if($prod_ids){
                    $products = wc_get_products(array(
                        'include' => $prod_ids,
                    ));
                    foreach($products as $product) {
                    ?>
                <div class="left col-lg-12">
                        <img src="<?php echo get_the_post_thumbnail_url($product->get_id());?>">
                    <div class="left_desr list_text">
                            <h4><?php echo $product->get_title();?></h4>
                        <p><?php the_content();?></p>
                        <a href="<?php the_permalink( $product->get_id() ); ?>" class="BN see_btn">
                            Подробнее о книге
                            <i class="fas fa-long-arrow-alt-right"></i>
                        </a>
                     </div>
                </div>

                <?php

                    }
                };
                ?>
            </div>
            <div class="button B">
                <a href="#">купить комплект-620 ₽ <!--<img src="img/arruw_up.png">--></a>
            </div>
        </div>
    </div>
</article>

<article id="partners">
    <div class="wrap">
        <div class="partners_list">
            <h2>Партнеры проекта</h2>
            <div class="partners_names">
                <div class="partners_sub">
                    <img src="<?php echo get_template_directory_uri() . '/img/Zumies-Logo@1X.png'?>" alt="part">
                    <img src="<?php echo get_template_directory_uri() . '/img/rosettastonelogowide@1X.png'?>" alt="part">
                    <img src="<?php echo get_template_directory_uri() . '/img/AlexandAni_logo_SEPT2014@1X.png'?>" alt="part">
                </div>
                <div class="partners_sub_sub">
                    <img src="<?php echo get_template_directory_uri() . '/img/_eastonlogo.jpg@1X.png'?>" alt="part">
                    <img src="<?php echo get_template_directory_uri() . '/img/chrisitian-louboutin@1X.png'?>" alt="part">
                    <img src="<?php echo get_template_directory_uri() . '/img/VIZIO_black@1X.png'?>" alt="part">
                </div>
            </div>
        </div>
        <div class="contacts">
            <a class="sub" href="#"><i class="bg-sub_gold"></i>подписаться</a>
            <div class="share">
                <a href="#"><img src="<?php echo get_template_directory_uri().'/img/share_gold.png'?>"></a>
                    <p>поделиться</p>
            <?php
            $socials = get_field('social_icons');

            foreach ($socials as $social){

           ?>
                <a href="#"><i class="<?php echo $social; ?>"></i></a>

                <!--<a href="#"></a><a href="#"><i class="bg-vk_gold"></i></a><div class="fot_links_block"><a href="#"><i class="bg-fb_gold"></i></a>
                    <a href="#"><i class="bg-twit_gold"></i></a><a href="#"><i class="bg-ins_gold"></i></a></div><a href="#"><i class="bg-tel_gold"></i></a><a href="#"><i class="bg-viber_gold"></i></a><a href="#"><i class="bg-whatUp_gold"></i></a>-->
            <?php } ?>

            </div>
            <a class="email" href="#"><img src="<?php echo get_template_directory_uri() . '/img/mail_gold.png'?>">отправить на email</a>
        </div>
    </div>
</article>
<?php get_footer();