

<footer>
    <div class="container">
        <div class="row">
            <div class="fot_nav col-lg-9 col-md-6">

                <?php $arg = array(
                    'theme_location' => 'footer'
                );

                wp_nav_menu( $arg );?>

            </div>
            <div class="fot_links col-lg-3 col-md-6">
                <div class="link_box"><a href=""><img src="<?php echo get_template_directory_uri() . '/img/fb.png'?>"></a> </div>
                <div class="link_box"><a href=""><img src="<?php echo get_template_directory_uri() . '/img/twit.png'?>"></a> </div>
                <div class="link_box"><a href=""><img src="<?php echo get_template_directory_uri() . '/img/insta.png'?>"></a> </div>
                <div class="link_box"><a href=""><img src="<?php echo get_template_directory_uri() . '/img/vk.png'?>"></a> </div>
                <div class="link_box"><a href=""><img src="<?php echo get_template_directory_uri() . '/img/utube.png'?>"></a> </div>
            </div>
        </div>
    </div>
    <div class="credits">
        <p>Книжный клуб Ос © 2007–2018</p>
    </div>
</footer>


<!--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>-->

<?php wp_footer(); ?>
</body>
</html>