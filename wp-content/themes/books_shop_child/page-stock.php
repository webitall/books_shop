

<?php
get_header();
$query = new WP_Query();
if ( $query -> have_posts() ) : ?>
    <?php while ( $query ->have_posts() ) : the_post(); ?>
        <p class="content_text"><?php the_content(); ?></p>
    <?php endwhile;
endif;
if (have_rows('my_stock_img')):
    while (have_rows('my_stock_img')) : the_row();?>

        <div class="drow_box">
            <a href="<?php get_the_permalink();?>"><img src="<?php echo get_sub_field('my_stock_img_2'); ?>"></a>
        </div>

<?php
    endwhile;
else :
    echo 'no rows found';
endif;

get_footer();








