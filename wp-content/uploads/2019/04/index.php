<!doctype html>
<html lang="ru" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <!-- <style>
        @import rel('https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700&subset=cyrillic-ext');
    </style>-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,700" rel="stylesheet">
    <link href="https://cdn.rawgit.com/mfd/f3d96ec7f0e8f034cc22ea73b3797b59/raw/856f1dbb8d807aabceb80b6d4f94b464df461b3e/gotham.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="fonts/fonts.css">
    <link rel="stylesheet" href="style/style.css">
    <link rel="stylesheet" href="media/media.css">

    <title>main_page</title>
</head>
<body>
    <div class="first_screen">
        <div class="wrap">
        <div class="container">
            <div class="row">
                <header>
                    <div class="header">
                        <div class="logo">
                            <a href="#"><img class="logo_img" src="img/logo.png"></a>
                            <!--<div class="nemu_btn">-->
                                <p class="menu"><a href="#">menu</a></p>
                                <img class="burger_menu" src="img/menu.png">
                         <!--   </div>-->
                        </div>
                        <div class="nav">
                            <ul>
                                <li><a href="#">книги</a></li>
                                <li><a href="#">розыгрыши</a></li>
                                <li><a href="#">пресс-центр</a></li>
                                <li><a href="#">о книжном клубе</a></li>
                            </ul>
                        </div>
                        <div>
                            <div class="log_in">
                                <img class="login" src="img/logIn.png">
                                <p><a href="#">войти</a></p>
                                <img class="search" src="img/search.png">
                            </div>
                        </div>
                    </div>
                </header>
                <div class="represent">
                    <div class="represent_block">
                        <img src="img/award.png">
                        <div class="represent_text">
                            <h4>Литературный конкурс
                                </h4>
                            <p>|| Всероссийский поэтический<br>
                                конкурс “Лого-рифм"</p>
                        </div>
                    </div>
                    <div class="represent_block">
                        <img src="img/cashback.png">
                        <div class="represent_text">
                            <h4>100% cash back</h4>
                            <p>пригласи 5 друзей</p>
                            <span>Активный читатель</span>
                        </div>
                    </div>
                    <div class="represent_block">
                        <img src="img/abonement.png">
                        <div class="represent_text">
                            <h4>абонемент на год</h4>
                            <p>пригласи 10 друзей</p>
                            <span>Постоянный читатель</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<article id="second_screen">
    <div class="container">
        <div class="row">
            <div class="under_first_screen col-lg-12">
                <div class="about_complect">
                    <p><a href="#">О комплекте</a></p>
                </div>
                <div class="buy_complect">
                    <img src="img/basket_grey.png">
                    <p><a href="#">купить комплект</a></p>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="container">
        <div class="row">
            <div class="propose_block col-lg-4 offset-lg-1">
                <img src="img/propose_img.png">
                    <div class="links_block top">
                        <div class="link_box"><img src="img/odnkls.png" alt="soc_img"></div>
                        <div class="link_box"><img src="img/vk.png" alt="soc_img"></div>
                        <div class="link_box"><img src="img/fb.png" alt="soc_img"></div>
                        <div class="link_box"><img src="img/twit.png" alt="soc_img"></div>
                    </div>
                    <div class="links_block bottom">
                        <div class="link_box"><img src="img/insta.png" alt="soc_img"></div>
                        <div class="link_box"><img src="img/telega.png" alt="soc_img"></div>
                        <div class="link_box"><img src="img/vib.png" alt="soc_img"></div>
                        <div class="link_box"><img src="img/hz.png" alt="soc_img"></div>
                    </div>
            </div>
                <div class="propose_content col-lg-6">
                    <span>#</span>
                    <h1>Архитектура, интерьер</h1>
                    <p class="propose_content_text">Искусство и наука строить, проектировать здания и сооружения, а также сама совокупность зданий и сооружений, создающих пространственную среду для жизни и деятельности человека</p>
                        <div class="prise">
                            <p>Цена:</p><br>
                            620
                            <p>₽</p>
                        </div>
                    <div class="propose_content_info cash_back">
                        <img src="img/cash_back.png">
                        <p>Cash back 100% </p>
                        <a href="#info_modal"> <img class="info_img" src ="img/info.png"> </a>
                        <div id="info_modal">
                            <p>Lorem ipsum dl velit voluptatibus? Deleniti distinctio labore laborum omnis sunt.</p>
                        </div>
                    </div>
                    <div class="propose_content_info buyer">
                        <img src="img/book.png">
                        <p>Статус: Постоянный читатель
                        </p>
                        <img src ="img/info.png">
                    </div>
                    <div class="propose_content_info money">
                        <img src="img/gold_pig.png">
                        <p>Зарабатывай с нижным клубом Ос
                        </p>
                        <img src ="img/info.png">
                    </div>
                    <div class="buttons">
                        <a href="#" class="sale_btn B">
                            <img src="img/gold_basket.png">
                            <p class="btn_text">купить комплект</p>
                        </a>
                        <a href="#" class="books_btn B">
                            <img src="img/bookstorage.png">
                            <p class="btn_text">книги комплекта</p>
                        </a>
                    </div>
        </div>
        </div>
    </div>
</article>
    <article id="drow">
        <div class="wrap">
             <div class="container">
                <div class="row">
                   <div class="drow">
                       <span>#</span>
                       <h1>Розыгрыш/ покупателям полного пакета</h1>
                       <p>с 5 апреля по 5 июня 2018</p>
                       <div class="drow_boxes">
                           <div>
                                <div class="drow_box car">
                                    <img src="img/Kia-Sorento.png">
                                </div>
                               <div class="car_text ">
                                   <h3>KIA SORENTO</h3>
                                   <p class="drow_box_text">Главный приз розыгрыша</p>
                               </div>
                           </div>

                           <div>
                             <div class="drow_box tablet">
                                   <img src="img/huaweimediapad.png">
                             </div>
                                 <div class="tablet_text">
                                     <h3>1000 планшетов</h3>
                                     <p class="drow_box_text">Читай везде где хочешь</p>
                                 </div>

                           </div>
                       </div>
<!--                       <div class="drow_box_content">-->
                          <!-- <div class="car_text">
                               <h3>KIA SORENTO</h3>
                               <p class="drow_box_text">Главный приз розыгрыша</p>-->
                          <!-- <div class="tablet_text">
                       <h3>1000 планшетов</h3>
                       <p class="drow_box_text">Читай везде где хочешь</p>
                   </div>
                   </div>-->
                       <div class="more">
                           <a href="#" class="B"><p>Подробнее о розыгрыше</p>
                           </a>
                       </div>

                   </div>
                </div>
            </div>
        </div>
    </article>
    <article id="books_set">
        <h1>Комплект из 5 книг<span>#</span>Архитектура, интерьер</h1>
        <p>с 5 апреля по 5 июня 2018</p>
        <div class="container">
            <div class="row">
                <div class="books_set_cont">
                    <div class="book_img">
                        <img class="" src="img/book1.png">
                    </div>
                    <div class="books_set_cont_text">
                        <h4>Самое главное для архитекторов</h4>
                        <p>Этому не учат в вузах. В своей книге преподаватель Йен Джексон с помощью коллег собрал наиболее ценные советы и рекомендации, которые помогут выжить и преуспеть на современном рынке архитектуры.</p>
                        <div>
                            <a class="B" href="#"><p>Подробнее о книге</p></a>
                        </div>
                    </div>

                </div>
                <div class="books_list">
                        <div class="left">
                            <img src="img/arhit.png">
                        <div class="left_desr list_text">
                            <h4>Архитектура. История</h4>
                            <p>В этом красочном издании рассказывается о самых значимых архитектурных шедеврах мира...</p>
                            <a href="#" class="BN see_btn">
                                Подробнее о книге
                                <i class="fas fa-long-arrow-alt-right"></i>
                            </a>
                        </div>

                    </div>
                    <div class="right">
                            <img src="img/scien.png">
                        <div class="right_desr list_text">
                            <h4>Cовременная Архитектура</h4>
                            <p>Автор представил в своей книге 100 оригинальных творений современности, рассказал</p>
                            <a href="#" class="BN see_btn">
                                Подробнее о книге
                                <i class="fas fa-long-arrow-alt-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="books_list bottom_list">
                    <div class="left">
                        <img src="img/cover.png">
                        <div class="left_desr list_text">
                            <h4>Эстетика пустоты</h4>
                            <p>В книге рассматривается история современной архитектуры Центральной Азии. Основное внимание уделено...</p>
                            <a href="#" class="BN see_btn">
                                Подробнее о книге
                                <i class="fas fa-long-arrow-alt-right"></i>
                            </a>
                        </div>

                    </div>
                    <div class="right">
                        <img src="img/poetika.png">
                        <div class="right_desr list_text">
                            <h4>Поэтика пространства</h4>
                            <p>Классическая работа 1957 год известного французского феноменолога посвящена образам пространств</p>
                            <a href="#" class="BN see_btn">
                                Подробнее о книге
                                <i class="fas fa-long-arrow-alt-right"></i>
                            </a>
                        </div>
                    </div>

                </div>
                <div class="button B">
                    <a href="#">купить комплект-620 ₽ <!--<img src="img/arruw_up.png">--></a>
                </div>
            </div>
        </div>
    </article>
    <article id="partners">
        <div class="wrap">
            <div class="partners_list">
                <h2>Партнеры проекта</h2>
                    <div class="partners_names">
                        <div class="partners_sub">
                            <img src="img/Zumies-Logo@1X.png" alt="part">
                            <img src="img/rosettastonelogowide@1X.png" alt="part">
                            <img src="img/AlexandAni_logo_SEPT2014@1X.png" alt="part">
                        </div>
                        <div class="partners_sub_sub">
                            <img src="img/_eastonlogo.jpg@1X.png" alt="part">
                            <img src="img/chrisitian-louboutin@1X.png" alt="part">
                            <img src="img/VIZIO_black@1X.png" alt="part">
                        </div>
                    </div>
            </div>
            <div class="contacts">
                <a class="sub" href="#"><img src="img/sub.png"> подписаться</a>
                <div class="share"><a href="#"><img src="img/share.png"></a>поделиться<a href="#"><img src="img/odn.png"></a><a href="#"><img src="img/vk_gold.png"></a><div class="fot_links_block"><a href="#"><img src="img/fb_gold.png"></a>
                    <a href="#"><img src="img/tw_gl.png"></a><a href="#"><img src="img/insgl.png"></a></div><a href="#"><img src="img/tel.png"></a><a href="#"><img src="img/viber.png"></a><a href="#"><img src="img/hzg.png"></a></div>
                <a class="email" href="#"><img src="img/mail_gold.png">отправить на email</a>
            </div>
        </div>
    </article>
    <footer>
        <div class="container">
            <div class="row">
                <div class="fot_nav col-lg-9 col-md-6">
                    <ul>
                        <li><a href="">Реферальная программа</a></li>
                        <li><a href="">Покупателям</a></li>
                        <li><a href="">Вопрос-ответ</a></li>
                        <li><a href="">Пользовательское соглашение</a></li>
                    </ul>
                </div>
                <div class="fot_links col-lg-3 col-md-6">
                    <div class="link_box"><a href=""><img src="img/fb.png"></a> </div>
                    <div class="link_box"><a href=""><img src="img/twit.png"></a> </div>
                    <div class="link_box"><a href=""><img src="img/insta.png"></a> </div>
                    <div class="link_box"><a href=""><img src="img/vk.png"></a> </div>
                    <div class="link_box"><a href=""><img src="img/utube.png"></a> </div>
                </div>
            </div>
        </div>

    </footer>
    <div class="credits">
        <p>Книжный клуб Ос © 2007–2018</p>
    </div>





<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<!--<script src="js/jquery-3.3.1.min.js"></script>-->
</body>
</html>